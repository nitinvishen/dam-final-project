import PyPDF2 
import textract
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import os
import pickle

read_folder = "F:\\FPM\\Term 3\\Data Analytics\\Final Project\\Letters to Investors\\2016\\Q4\\"
save_folder = "F:\\FPM\\Term 3\\Data Analytics\\Final Project\\Letters to Investors\\Analysis\\"

all_files = os.listdir(read_folder)
    
letters = list()
for each_file in all_files:
    try:
        pdfFileObj = open(read_folder + each_file, 'rb')
        pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
        #print(pdfReader.numPages)
        page_no = 0
        letter_text = ""
        while page_no < pdfReader.numPages:
            pageObj = pdfReader.getPage(page_no)
            letter_text = letter_text + pageObj.extractText()
            page_no = page_no + 1
        letters.append([each_file, letter_text])
        pdfFileObj.close()
#        with open(save_folder+each_file+'.pickle', 'wb') as handle:
#            pickle.dump(letter_text, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
    except:
        print("Failed to read pdf file:" + each_file)
    
with open(save_folder+'2016Q4.pickle', 'wb') as handle:
            pickle.dump(letters, handle, protocol=pickle.HIGHEST_PROTOCOL)

