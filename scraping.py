# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 17:15:21 2018

@author: 51750004
"""

import requests
from bs4 import BeautifulSoup
import datetime
import time
from calendar import monthrange
import pickle
from urllib.request import urlopen
import re

#url = "https://vintagevalueinvesting.com/complete-list-q1-2017-hedge-fund-letters-investors/"
#url = "https://vintagevalueinvesting.com/complete-list-q2-2017-hedge-fund-letters-investors/"
#url = "https://vintagevalueinvesting.com/complete-list-q3-2017-hedge-fund-letters-investors/"
url = "https://vintagevalueinvesting.com/complete-list-q4-2017-hedge-fund-letters-investors/"

html_page = urlopen(url)
soup = BeautifulSoup(html_page, "lxml")
table1 = soup.findAll('table')
a_list = table1[0].findAll('a')

links = dict()
#key = "2017-Q1"
for a in a_list:
    links[a.text] = a.get('href')
    
folder = "F:\\FPM\\Term 3\\Data Analytics\\Final Project\\Letters to Investors\\2017\\Q4\\"
for key, value in links.items():
    r = requests.get(value)
    k = re.sub('[!@#$/]', '', key)
    with open(folder+k+".pdf",'wb') as f:
        f.write(r.content)

